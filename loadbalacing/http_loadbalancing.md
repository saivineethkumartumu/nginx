# HTTP Load Balancing
### Problem :
Need to Distribute load between the two are more HTTP Servers

### Solution :
Load balacing over HTTP servers done by using the upstream block

```
upstream apploadbal {
		server app1.example.com:80 weight=1;
		server app2.example.com:80 weight=2;
		server backup.example.com:80 backup;
}

server {
 location / {
	proxy_pass http://apploadbal;
 }
}
```
this configuration balances load across two HTTP servers on port 80 and defines one as a backup which is used when the primary two servers are unavailable/down/offline. The weight parameter instructs nginx to pass twice as many requests to the second server, by default the weight parameter set to 1 
